package net.gcandido.coinbr.portfolio.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;

import net.gcandido.coinbr.common.data.DatabaseProvider;
import net.gcandido.coinbr.common.models.PortfolioItem;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tgcandido on 01/07/17.
 */

public class PortfolioDAO {
    private SQLiteDatabase db;
    private DatabaseProvider dbProvider;
    private static PortfolioDAO singleton;

    private PortfolioDAO(Context context){
        dbProvider = new DatabaseProvider(context);
    }

    public static PortfolioDAO getInstance(Context context){
        if (singleton == null){
            singleton = new PortfolioDAO(context);
        }
        return singleton;
    }

    public boolean insert(PortfolioItem item){
        db = dbProvider.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseProvider.PORTFOLIO_BRLVALUE, item.getBrlValue());
        values.put(DatabaseProvider.PORTFOLIO_CURRENCYID, item.getCurrencyId());
        values.put(DatabaseProvider.PORTFOLIO_QUANTITY, item.getQuantity());

        long result = db.insert(DatabaseProvider.PORTFOLIO_TABLE, null, values);
        db.close();

        if (result == -1)
            return false;
        return true;
    }

    public void update(int id, PortfolioItem item){
        String where = DatabaseProvider.PORTFOLIO_ID + " = " + id;
        db = dbProvider.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseProvider.PORTFOLIO_QUANTITY, item.getQuantity());
        values.put(DatabaseProvider.PORTFOLIO_BRLVALUE, item.getBrlValue());

        db.update(DatabaseProvider.PORTFOLIO_TABLE, values, where, null);
        db.close();

    }

    public PortfolioItem findById(int id){

        String where = DatabaseProvider.PORTFOLIO_ID + " = " + id;
        Cursor cursor;
        String[] fields =  {DatabaseProvider.PORTFOLIO_ID,
                DatabaseProvider.PORTFOLIO_BRLVALUE,
                DatabaseProvider.PORTFOLIO_CURRENCYID,
                DatabaseProvider.PORTFOLIO_QUANTITY};

        db = dbProvider.getReadableDatabase();
        cursor = db.query(DatabaseProvider.PORTFOLIO_TABLE, fields, where , null, null, null, null, null);
        if (cursor.moveToFirst())
            return factory(cursor);
        return null;
    }

    public List<PortfolioItem> getAll(){
        List<PortfolioItem> list = new ArrayList<>();
        Cursor cursor;
        String[] fields =  {DatabaseProvider.PORTFOLIO_ID,
                DatabaseProvider.PORTFOLIO_BRLVALUE,
                DatabaseProvider.PORTFOLIO_CURRENCYID,
                DatabaseProvider.PORTFOLIO_QUANTITY};

        db = dbProvider.getReadableDatabase();
        cursor = db.query(DatabaseProvider.PORTFOLIO_TABLE, fields, null, null, null, null, null, null);


        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                list.add(factory(cursor));
                cursor.moveToNext();
            }
        }
        db.close();
        return list;
    }

    private PortfolioItem factory(Cursor cursor){
        if (cursor != null && !cursor.isAfterLast()){
            int id = cursor.getInt(cursor.getColumnIndex(DatabaseProvider.PORTFOLIO_ID));
            double brlValue = cursor.getDouble(cursor.getColumnIndex(DatabaseProvider.PORTFOLIO_BRLVALUE));
            double quantity = cursor.getDouble(cursor.getColumnIndex(DatabaseProvider.PORTFOLIO_QUANTITY));
            String currencyId = cursor.getString(cursor.getColumnIndex(DatabaseProvider.PORTFOLIO_CURRENCYID));

            return new PortfolioItem(
                    id,
                    quantity,
                    brlValue,
                    currencyId
            );
        } else {
            return null;
        }
    }

    public void delete(int id) {
        String where = DatabaseProvider.PORTFOLIO_ID + " = " + id;
        db = dbProvider.getReadableDatabase();
        db.delete(DatabaseProvider.PORTFOLIO_TABLE, where, null);
        db.close();
    }
}