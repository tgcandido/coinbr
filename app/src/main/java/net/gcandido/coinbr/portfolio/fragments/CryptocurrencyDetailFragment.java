package net.gcandido.coinbr.portfolio.fragments;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.gcandido.coinbr.R;
import net.gcandido.coinbr.common.models.PortfolioItem;
import net.gcandido.coinbr.portfolio.activities.CryptocurrencyDetailActivity;
import net.gcandido.coinbr.portfolio.activities.CryptocurrencyListActivity;
import net.gcandido.coinbr.portfolio.data.PortfolioDAO;

/**
 * A fragment representing a single Cryptocurrency detail screen.
 * This fragment is either contained in a {@link CryptocurrencyListActivity}
 * in two-pane mode (on tablets) or a {@link CryptocurrencyDetailActivity}
 * on handsets.
 */
public class CryptocurrencyDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "id";

    /**
     * The dummy content this fragment is presenting.
     */
    private PortfolioItem mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CryptocurrencyDetailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = PortfolioDAO.getInstance(getContext())
                    .findById(Integer.valueOf(getArguments().getString(ARG_ITEM_ID)));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getCurrencyId());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.cryptocurrency_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.cryptocurrency_detail)).setText(mItem.getCurrencyId());
        }

        return rootView;
    }
}
