package net.gcandido.coinbr.portfolio.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

import net.gcandido.coinbr.R;
import net.gcandido.coinbr.common.models.Currency;
import net.gcandido.coinbr.common.models.PortfolioItem;
import net.gcandido.coinbr.common.util.BaseURL;
import net.gcandido.coinbr.common.dispatchers.CryptocurrencyDispatcher;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by thiago.candido on 01/07/2017.
 */

public class AddCryptocurrencyToPortfolioFragment extends DialogFragment {



    public interface AddCryptocurrencyDialogListener {
        void onDialogPositiveClick(PortfolioItem item);
    }

    private AddCryptocurrencyDialogListener listener;
    private PortfolioItem item;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a;

        if (context instanceof Activity) {
            a = (Activity) context;
            listener = (AddCryptocurrencyDialogListener) a;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.dialog_add_crypto_portfolio, null))
                .setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String currencyId = ((EditText) getDialog().findViewById(R.id.symbol)).getText().toString();
                        item = createItem(currencyId);

                        if (currencyId != null && !currencyId.isEmpty()){
                            getCurrency(currencyId);
                        } else {
                            showError("Insira o nome da criptomoeda");
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        return builder.create();
    }


    private void getCurrency(String id){
        final ProgressDialog loading = ProgressDialog.show(getContext(),"Carregando","Isso pode demorar um minuto",false,false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseURL.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CryptocurrencyDispatcher dispatcher = retrofit.create(CryptocurrencyDispatcher.class);
        Call<List<Currency>> call = dispatcher.loadCryptocurrency(id.toLowerCase(), "BRL");
        call.enqueue(new Callback<List<Currency>>() {
            @Override
            public void onResponse(Call<List<Currency>> call, Response<List<Currency>> response) {
                loading.dismiss();
                if (response.code() == 404){
                    showError("Criptomoeda não encontrada.");
                } else {
                    List<Currency> items = response.body();
                    if (items != null && items.size() > 0){
                        listener.onDialogPositiveClick(item);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Currency>> call, Throwable t) {
                showError("Algo deu errado :(");
            }
        });
    }

    private PortfolioItem createItem(String currencyId){
        double quantity = Double.valueOf(
                ((EditText) getDialog().findViewById(R.id.add_crypto_value)).getText().toString());
        double brlValue = Double.valueOf(
                ((EditText) getDialog().findViewById(R.id.add_crypto_brlValue)).getText().toString());
        //Date purchaseDate = Date.valueOf(
                //((EditText) getDialog().findViewById(R.id.purchaseDate)).getText().toString());

        return new PortfolioItem(quantity, brlValue, currencyId);
    }

    private void showError(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}