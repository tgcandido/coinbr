package net.gcandido.coinbr;

import android.app.Application;
import android.content.Context;

/**
 * Created by thiago.candido on 27/06/2017.
 */

public class CoinBr  extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
