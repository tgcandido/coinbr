package net.gcandido.coinbr.top10.dispatchers;

import net.gcandido.coinbr.common.models.Currency;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by thiago.candido on 27/06/2017.
 */

public interface CurrencyListInterface {
    @GET("ticker")
    Call<List<Currency>> loadCryptocurrencies(@Query("limit") Integer limit, @Query("convert") String convert);
}
