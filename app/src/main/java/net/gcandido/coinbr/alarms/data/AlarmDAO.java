package net.gcandido.coinbr.alarms.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.gcandido.coinbr.common.data.DatabaseProvider;
import net.gcandido.coinbr.common.enums.Condition;
import net.gcandido.coinbr.common.models.Alarm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tgcandido on 02/07/17.
 */

public class AlarmDAO {
    private SQLiteDatabase db;
    private DatabaseProvider dbProvider;
    private static AlarmDAO singleton;

    private AlarmDAO(Context context){
        dbProvider = new DatabaseProvider(context);
    }

    public static AlarmDAO getInstance(Context context){
        if (singleton == null){
            singleton = new AlarmDAO(context);
        }
        return singleton;
    }

    public boolean insert(Alarm item){
        db = dbProvider.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseProvider.ALARM_BRLVALUE, item.getBrlValue());
        values.put(DatabaseProvider.ALARM_CURRENCYID, item.getCurrencyId());
        values.put(DatabaseProvider.ALARM_CONDITION, item.getConditionString());

        long result = db.insert(DatabaseProvider.ALARM_TABLE, null, values);
        db.close();

        if (result == -1)
            return false;
        return true;
    }

    public void update(int id, Alarm item){
        String where = DatabaseProvider.ALARM_ID + " = " + id;
        db = dbProvider.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseProvider.ALARM_BRLVALUE, item.getBrlValue());
        values.put(DatabaseProvider.ALARM_CONDITION, item.getConditionString());

        db.update(DatabaseProvider.ALARM_TABLE, values, where, null);
        db.close();

    }

    public Alarm findById(int id){

        String where = DatabaseProvider.ALARM_ID + " = " + id;
        Cursor cursor;
        String[] fields =  {DatabaseProvider.ALARM_ID,
                DatabaseProvider.ALARM_CONDITION,
                DatabaseProvider.ALARM_CURRENCYID,
                DatabaseProvider.ALARM_BRLVALUE};

        db = dbProvider.getReadableDatabase();
        cursor = db.query(DatabaseProvider.ALARM_TABLE, fields, where , null, null, null, null, null);
        if (cursor.moveToFirst())
            return factory(cursor);
        return null;
    }

    public List<Alarm> getAll(){
        List<Alarm> list = new ArrayList<>();
        Cursor cursor;
        String[] fields =  {DatabaseProvider.ALARM_ID,
                DatabaseProvider.ALARM_CONDITION,
                DatabaseProvider.ALARM_CURRENCYID,
                DatabaseProvider.ALARM_BRLVALUE};
        db = dbProvider.getReadableDatabase();
        cursor = db.query(DatabaseProvider.ALARM_TABLE, fields, null, null, null, null, null, null);


        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                list.add(factory(cursor));
                cursor.moveToNext();
            }
        }
        db.close();
        return list;
    }

    private Alarm factory(Cursor cursor){
        if (cursor != null && !cursor.isAfterLast()){
            int id = cursor.getInt(cursor.getColumnIndex(DatabaseProvider.ALARM_ID));
            double brlValue = cursor.getDouble(cursor.getColumnIndex(DatabaseProvider.ALARM_BRLVALUE));
            Condition condition = Condition.valueOf(
                    cursor.getString(cursor.getColumnIndex(DatabaseProvider.ALARM_CONDITION)));
            String currencyId = cursor.getString(cursor.getColumnIndex(DatabaseProvider.ALARM_CURRENCYID));

            return new Alarm(
                    id,
                    currencyId,
                    condition,
                    brlValue
            );
        } else {
            return null;
        }
    }

    public void delete(int id) {
        String where = DatabaseProvider.ALARM_ID+ " = " + id;
        db = dbProvider.getReadableDatabase();
        db.delete(DatabaseProvider.ALARM_TABLE, where, null);
        db.close();
    }
}
