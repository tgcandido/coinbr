package net.gcandido.coinbr.alarms.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import net.gcandido.coinbr.R;
import net.gcandido.coinbr.alarms.activities.AlarmListActivity;
import net.gcandido.coinbr.common.dispatchers.CryptocurrencyDispatcher;
import net.gcandido.coinbr.common.enums.Condition;
import net.gcandido.coinbr.common.models.Alarm;
import net.gcandido.coinbr.common.models.Currency;
import net.gcandido.coinbr.common.models.PortfolioItem;
import net.gcandido.coinbr.common.util.BaseURL;
import net.gcandido.coinbr.portfolio.fragments.AddCryptocurrencyToPortfolioFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpsertAlarmFragment extends DialogFragment {

    public interface AlarmUpsertedDialogListener {
        void onDialogPositiveClick(Alarm item);
    }

    private Alarm item;
    private AlarmUpsertedDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a;

        if (context instanceof Activity) {
            a = (Activity) context;
            listener = (AlarmUpsertedDialogListener) a;
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();


        builder.setView(inflater.inflate(R.layout.fragment_upsert_alarm, null))
                .setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        buildAlarm();
                        loadCurrency();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Alarm selectedAlarm = ((AlarmListActivity) getActivity()).getSelectedAlarm();
        if (selectedAlarm != null) {
            item = selectedAlarm;
            ((EditText) getDialog().findViewById(R.id.alarm_brl_value)).setText(String.valueOf(item.getBrlValue()));
            ((EditText) getDialog().findViewById(R.id.alarm_currency_name)).setText(item.getCurrencyId());
            ((Spinner) getDialog().findViewById(R.id.alarm_condition)).setSelection(item.getCondition().ordinal());
        } else {
            item = new Alarm("", Condition.HIGHER, 0.00);
        }
    }

    private void buildAlarm(){
        Double brlValue = Double.valueOf(
                ((EditText) getDialog().findViewById(R.id.alarm_brl_value)).getText().toString());
        String currencyId = ((EditText) getDialog().findViewById(R.id.alarm_currency_name)).getText().toString();
        Condition condition = Condition.values()[((Spinner) getDialog().findViewById(R.id.alarm_condition)).getSelectedItemPosition()];

        item.setBrlValue(brlValue);
        item.setCurrencyId(currencyId);
        item.setCondition(condition);
    }

    private void loadCurrency(){
        final ProgressDialog loading = ProgressDialog.show(getContext(),"Carregando","Isso pode demorar um minuto",false,false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseURL.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CryptocurrencyDispatcher dispatcher = retrofit.create(CryptocurrencyDispatcher.class);
        Call<List<Currency>> call = dispatcher.loadCryptocurrency(item.getCurrencyId().toLowerCase(), "BRL");
        call.enqueue(new Callback<List<Currency>>() {
            @Override
            public void onResponse(Call<List<Currency>> call, Response<List<Currency>> response) {
                loading.dismiss();
                if (response.code() == 404){
                    showError("Criptomoeda não encontrada.");
                } else {
                    List<Currency> items = response.body();
                    if (items != null && items.size() > 0){
                        listener.onDialogPositiveClick(item);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Currency>> call, Throwable t) {
                showError("Algo deu errado :(");
            }
        });
    }


    private void showError(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
