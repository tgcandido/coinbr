package net.gcandido.coinbr.common.dispatchers;

import net.gcandido.coinbr.common.models.Currency;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by thiago.candido on 01/07/2017.
 */

public interface CryptocurrencyDispatcher {
    @GET("ticker/{currencyId}/")
    Call<List<Currency>> loadCryptocurrency(@Path("currencyId") String currency, @Query("convert") String convert);
}
