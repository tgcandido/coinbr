package net.gcandido.coinbr.common.models;

import java.sql.Date;
import java.util.UUID;

/**
 * Created by tgcandido on 01/07/17.
 */

public class PortfolioItem {
    private int id;

    public int getId() {
        return id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getBrlValue() {
        return brlValue;
    }

    public void setBrlValue(double brlValue) {
        this.brlValue = brlValue;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }


    public PortfolioItem(int id, double quantity, double brlValue, String currencyId) {
        this.id = id;
        this.quantity = quantity;
        this.brlValue = brlValue;
        this.currencyId = currencyId;
    }

    public PortfolioItem(double quantity, double brlValue, String currencyId) {
        this.quantity = quantity;
        this.brlValue = brlValue;
        this.currencyId = currencyId;
    }

    private double brlValue;
    private String currencyId;
    private double quantity;
}
