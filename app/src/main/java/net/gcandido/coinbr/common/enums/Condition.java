package net.gcandido.coinbr.common.enums;

/**
 * Created by tgcandido on 02/07/17.
 */

public enum Condition {
    LOWER,
    HIGHER
}
