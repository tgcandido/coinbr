package net.gcandido.coinbr.common.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tgcandido on 01/07/17.
 */

public class DatabaseProvider extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "coinbr.db";
    public static int VERSION = 2;
    public static String PORTFOLIO_TABLE = "portfolio_items";
    public static String PORTFOLIO_ID = "id";
    public static String PORTFOLIO_QUANTITY = "quantity";
    public static String PORTFOLIO_BRLVALUE= "brl_value";
    public static String PORTFOLIO_CURRENCYID = "currency_id";
    public static String PORTFOLIO_PURCHASEDATE = "purchase_date";
    public static String ALARM_TABLE = "alarms";
    public static String ALARM_ID = "id";
    public static String ALARM_CURRENCYID = "currency_id";
    public static String ALARM_CONDITION = "condition";
    public static String ALARM_BRLVALUE = "brl_value";

    public DatabaseProvider(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String portfolioTable = "CREATE TABLE " + PORTFOLIO_TABLE + " ( " +
                PORTFOLIO_ID + " integer primary key autoincrement, " +
                PORTFOLIO_QUANTITY + " real, " +
                PORTFOLIO_BRLVALUE + " real, " +
                PORTFOLIO_CURRENCYID + " text); ";

        String alarmTable = "CREATE TABLE " + ALARM_TABLE + " ( " +
                ALARM_ID + " integer primary key autoincrement, " +
                ALARM_BRLVALUE + " real, " +
                ALARM_CONDITION + " text, "+
                ALARM_CURRENCYID + " text); ";


        db.execSQL(portfolioTable);
        db.execSQL(alarmTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PORTFOLIO_TABLE + ";");
        db.execSQL("DROP TABLE IF EXISTS " + ALARM_TABLE + ";");
        onCreate(db);
    }
}
