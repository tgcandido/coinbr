package net.gcandido.coinbr.common.models;

/**
 * Created by thiago.candido on 27/06/2017.
 */

public class Currency {
        public final String id;
        public final String name;
        public final String symbol;
        public final long rank;
        public final double price_usd;
        public final double price_btc;
        public final double day_volume_usd;
        public final double market_cap_usd;
        public final double available_supply;
        public final double total_supply;
        public final double percent_change_1h;
        public final double percent_change_24h;
        public final double percent_change_7d;
        public final String last_updated;
        public final double price_brl;
        public final double day_volume_brl;
        public final double market_cap_brl;

        public Currency(String id, String name, String symbol, long rank, double price_usd, long price_btc, long day_volume_usd, long market_cap_usd, long available_supply, long total_supply, double percent_change_1h, double percent_change_24h, double percent_change_7d, String last_updated, double price_brl, double day_volume_brl, long market_cap_brl){
            this.id = id;
            this.name = name;
            this.symbol = symbol;
            this.rank = rank;
            this.price_usd = price_usd;
            this.price_btc = price_btc;
            this.day_volume_usd = day_volume_usd;
            this.market_cap_usd = market_cap_usd;
            this.available_supply = available_supply;
            this.total_supply = total_supply;
            this.percent_change_1h = percent_change_1h;
            this.percent_change_24h = percent_change_24h;
            this.percent_change_7d = percent_change_7d;
            this.last_updated = last_updated;
            this.price_brl = price_brl;
            this.day_volume_brl = day_volume_brl;
            this.market_cap_brl = market_cap_brl;
        }
    }

