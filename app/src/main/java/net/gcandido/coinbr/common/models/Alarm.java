package net.gcandido.coinbr.common.models;

import net.gcandido.coinbr.common.enums.Condition;

/**
 * Created by tgcandido on 02/07/17.
 */

public class Alarm {
    private int id;
    private String currencyId;
    private Condition condition;
    private double brlValue;

    public Alarm(String currencyId, Condition condition, double brlValue) {
        this.currencyId = currencyId;
        this.condition = condition;
        this.brlValue = brlValue;
    }

    public Alarm(int id, String currencyId, Condition condition, double brlValue) {
        this.id = id;
        this.currencyId = currencyId;
        this.condition = condition;
        this.brlValue = brlValue;

    }

    public int getId() {
        return id;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Condition getCondition() {
        return condition;
    }

    public String getConditionString(){
        return condition.name();
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public double getBrlValue() {
        return brlValue;
    }

    public void setBrlValue(double brlValue) {
        this.brlValue = brlValue;
    }
}
